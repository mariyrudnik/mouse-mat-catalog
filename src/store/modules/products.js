export default {
  state: () => ({
    items: [
      {
        id: 1,
        name: 'RZTK Galaxy',
        price: 99,
        height: 250,
        width: 300,
        color: 'print',
        shape: 'square',
        img: 'src/assets/photo/1.webp'
      },
      {
        id: 2,
        name: 'RZTK Cyber City',
        price: 299,
        height: 400,
        width: 900,
        color: 'print',
        shape: 'rectangle',
        img: 'src/assets/photo/2.webp'
      },
      {
        id: 3,
        name: 'A4Tech X7-500MP Speed',
        price: 349,
        height: 400,
        width: 437,
        color: 'black',
        shape: 'square',
        img: 'src/assets/photo/3.webp'
      },
      {
        id: 4,
        name: 'Logitech Mouse Pad Studio Series',
        price: 379,
        height: 200,
        width: 230,
        color: 'blue',
        shape: 'square',
        img: 'src/assets/photo/4.webp'
      },
      {
        id: 5,
        name: 'Gembird',
        price: 169,
        height: 220,
        width: 260,
        color: 'black',
        shape: 'circle',
        img: 'src/assets/photo/5.webp'
      },
      {
        id: 6,
        name: 'HyperX Pulsefire Mat RGB Speed',
        price: 2099,
        height: 420,
        width: 900,
        color: 'black',
        shape: 'rectangle',
        img: 'src/assets/photo/6.webp',
        light: true
      },
      {
        id: 7,
        name: 'Hator Tonn Evo L.E. Speed Control',
        price: 859,
        height: 420,
        width: 500,
        color: 'print',
        shape: 'square',
        img: 'src/assets/photo/7.webp'
      },
      {
        id: 8,
        name: 'RZTK Anonymous',
        price: 299,
        height: 400,
        width: 900,
        color: 'print',
        shape: 'rectangle',
        img: 'src/assets/photo/8.webp'
      },
      {
        id: 9,
        name: '2E Gaming M Speed/Control',
        price: 279,
        height: 275,
        width: 360,
        color: 'white',
        shape: 'square',
        img: 'src/assets/photo/9.webp'
      },
      {
        id: 10,
        name: 'Defender XXL Light Speed',
        price: 685,
        height: 400,
        width: 450,
        color: 'black',
        shape: 'square',
        img: 'src/assets/photo/10.webp',
        light: true
      },
      {
        id: 11,
        name: 'RZTK ProArt',
        price: 199,
        height: 300,
        width: 600,
        color: 'print',
        shape: 'rectangle',
        img: 'src/assets/photo/11.webp'
      },
      {
        id: 12,
        name: 'Logitech G840 XL KDA',
        price: 2299,
        height: 400,
        width: 900,
        color: 'print',
        shape: 'rectangle',
        img: 'src/assets/photo/12.webp'
      },
      {
        id: 13,
        name: 'U&P Elder Dragon R800 Mixed',
        price: 299,
        height: 300,
        width: 800,
        color: 'print',
        shape: 'rectangle',
        img: 'src/assets/photo/13.webp'
      },
      {
        id: 14,
        name: 'Vertux SwiftPad-L',
        price: 599,
        height: 252,
        width: 328,
        color: 'black',
        shape: 'square',
        img: 'src/assets/photo/14.webp',
        light: true
      },
      {
        id: 15,
        name: 'Gembird MP-GEL',
        price: 159,
        height: 260,
        width: 220,
        color: 'grey',
        shape: 'circle',
        img: 'src/assets/photo/15.webp'
      },
      {
        id: 16,
        name: 'Gembird MP-N3',
        price: 65,
        height: 205,
        width: 205,
        color: 'brown',
        shape: 'circle',
        img: 'src/assets/photo/16.webp'
      },
    ]
  }),
  mutations: {},
  actions: {},
  getters: {
    getItemById: (state) => (id) => {
      return state.items.find(item => item.id === +id)
    },
    getMaxAndMinPrise: (state) => {
      let prices = state.items.map(item => item.price)
      return {
        min: Math.min(...prices),
        max: Math.max(...prices),
      }
    },
    getFilteredItems: (state) => (filters) => {
      if (filters.color.length || filters.light || filters.shape.length || filters.price.filtered) {
        let filteredItems = [...state.items]
        if (filters.color.length) {
          filteredItems = filteredItems.filter(item => filters.color.includes(item.color))
        }
        if (filters.light) {
          filteredItems = filteredItems.filter(item => item.light === filters.light)
        }
        if (filters.shape.length) {
          filteredItems = filteredItems.filter(item => filters.shape.includes(item.shape))
        }
        if (filters.price.filtered[0] > filters.price.default[0] || filters.price.filtered[1] < filters.price.default[1]) {
          filteredItems = filteredItems.filter(item => item.price >= filters.price.filtered[0] && item.price <= filters.price.filtered[1])
        }
        return filteredItems
      } else {
        return state.items
      }
    }
  }
}
