import { createStore } from 'vuex'
import products from '@/store/modules/products'

const store = createStore({
  state () {
    return {}
  },
  modules : {
    products
  }
})

export default store
